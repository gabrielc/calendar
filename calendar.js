let Calendar = (function() {
  let container, label, argHolidays;
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  function init(newWrap) {
    container = $(newWrap || "#my-calendar");
    label = container.find("#label");
    container.find("#prev").bind("click.calendar", function() {
      switchMonth(false, null, null, argHolidays);
    });
    container.find("#next").bind("click.calendar", function() {
      switchMonth(true, null, null, argHolidays);
    });
    label.bind("click", function() {
      switchMonth(null, new Date().getMonth(), new Date().getFullYear(), argHolidays);
    });
    const URL = "http://nolaborables.com.ar/api/v2/feriados/" + new Date().getFullYear();
    label.click();
    makeAjaxCall(URL, "GET").then(function(respJson){
      argHolidays = respJson;
      switchMonth(null, new Date().getMonth(), new Date().getFullYear(), argHolidays);
    }, function(reason){
      console.log("error in processing your request", reason);
    });

  }

  function makeAjaxCall(url, methodType, callback){
    return $.ajax({
      url : url,
      method : methodType,
      dataType : "json"
    });
  }

  function switchMonth(next, month, year, holidays) {
    let curr = label
        .text()
        .trim()
        .split(" "),
      calendar,
      tempYear = parseInt(curr[1], 10);
    month =
      month ||
      (next
        ? curr[0] === "December" ? 0 : months.indexOf(curr[0]) + 1
        : curr[0] === "January" ? 11 : months.indexOf(curr[0]) - 1);
    year =
      year ||
      (next && month === 0
        ? tempYear + 1
        : !next && month === 11 ? tempYear - 1 : tempYear);

    calendar = createCal(year, month, holidays);
    $("#cal-frame", container)
      .find(".curr")
      .removeClass("curr")
      .addClass("temp")
      .end()
      .prepend(calendar.calendar())
      .find(".temp")
      .fadeOut("slow", function() {
        $(this).remove();
      });

    $("#label").text(calendar.label);
  }

  function adjustCalendar(calendar) {
    for (i = 0; i < calendar[5].length; i++) {
      if (calendar[5][i] !== "") {
        calendar[4][i] =
          "<span>" +
          calendar[4][i] +
          "</span><span>" +
          calendar[5][i] +
          "</span>";
      }
    }
    return calendar.slice(0, 5);
  }

  function fillCalendar(calendar) {
    for (i = 0; i < calendar.length; i++) {
      calendar[i] = "<tr><td>" + calendar[i].join("</td><td>") + "</td></tr>";
    }
    return $("<table>" + calendar.join("") + "</table>").addClass("curr");
  }

  function createCal(year, month, holidays) {
    let day = 1,
      i,
      j,
      haveDays = true,
      monthHolidays = false,
      startDay = new Date(year, month, day).getDay(),
      daysInMonths = [
        31,
        (year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? 29 : 28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31
      ],
      calendar = [];

    if (createCal.cache[year]) {
      if ((createCal.cache[year][month]) && (createCal.cache[year][month]['holidays'] === true)) {
        return createCal.cache[year][month];
      }
    } else {
      createCal.cache[year] = {};
    }
    i = 0;
    while (haveDays) {
      calendar[i] = [];
      for (j = 0; j < 7; j++) {
        if (i === 0) {
          if (j === startDay) {
            calendar[i][j] = day++;
            startDay++;
          }
        } else if (day <= daysInMonths[month]) {
          calendar[i][j] = day++;
        } else {
          calendar[i][j] = "";
          haveDays = false;
        }
        if (day > daysInMonths[month]) {
          haveDays = false;
        }
      }
      i++;
    }

    if (calendar[5]) {
      calendar = adjustCalendar(calendar);
    }
    calendar = fillCalendar(calendar);
    $("td:empty", calendar).addClass("nil");
    if (month === new Date().getMonth()) {
      $("td", calendar)
        .filter(function() {
          return $(this).text() === new Date().getDate().toString();
        })
        .addClass("today");
    }

    if ((holidays !== null) && (holidays !== undefined)) {
      for (item of holidays) {
        if (item['mes'] === month+1) {
          $("td", calendar)
            .filter(function() {
              return $(this).text() === item['dia'].toString();
            })
            .addClass("holiday");
        }
      }
      monthHolidays = true;
    }

    createCal.cache[year][month] = {
      calendar: function() {
        return calendar.clone();
      },
      label: months[month] + " " + year,
      holidays: monthHolidays
    };

    return createCal.cache[year][month];
  }

  createCal.cache = {};
  return {
    init: init
  };
})();
